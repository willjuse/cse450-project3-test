# CSE 450 Project 3
# Makefile

simplecalc:	simplecalc.o y.tab.o lex.yy.o
	g++ simplecalc.o y.tab.o lex.yy.o  -o simplecalc

lex.yy.c: simplecalc.l y.tab.h
	flex simplecalc.l

lex.yy.o: lex.yy.c
	gcc -c lex.yy.c

y.tab.c y.tab.h:  simplecalc.y
	bison --yacc --defines simplecalc.y

y.tab.o: y.tab.c y.tab.h
	gcc -c y.tab.c

simplecalc.o: simplecalc.cc y.tab.h
	g++ -c simplecalc.cc

test:
	./simplecalc < test1
	./simplecalc < test2
	./simplecalc < test3

clean:
	rm -f *.o simplecalc lex.yy* y.tab*
