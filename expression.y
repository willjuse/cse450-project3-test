%{
/*
 * Expression evaluator
*/
#include "stdio.h"
#include "math.h"

void answer(int n);
%}

%union {
	int	intval;
	double	dval;
	char *  sval;
}

%token<dval> NUM
%token<sval> STRING
%token<sval> POSNEWVAR
%token ERROR
%token<sval> STRTOPRINT
%token<sval> PRINT
%token<sval> SQRT
%token<sval> COMMENT

%token LPAREN RPAREN

%%
F :     		F '\n' S
| 				S;

S :     E              	{answer($<dval>$);};
S :     VAREXP       		{};
S :			COMMAND					{printf("\n"); /*print($<sval>1);*/};
S :			COMMENT;
S :     ;

VAREXP: 		NEWVAR E					{AddNewVariable($<sval>1, $<dval>2);}; /*printf("New variable added\n");*/

REALNUM: 	LPAREN E RPAREN 		{$<dval>$ = $<dval>2;}; /* evaluate expressions as soon as we can "(5+5)"-->"10" */
REALNUM: 	SQRT LPAREN E RPAREN	{$<dval>$ = sqrt($<dval>3);};

/* T1 operators are more important (higher precedence) than T2 operators */

E :			E '+' T2        	{$<dval>$ = $<dval>1 + $<dval>3; /*printf("Add: %d\n", $$);*/}
|    			E '-' T2        	{$<dval>$ = $<dval>1 - $<dval>3;}
|		 		E '<' T2				{$<dval>$ = (int)(($<dval>1) < ($<dval>3));}
|		 		E '>' T2				{$<dval>$ = (int)(($<dval>1) > ($<dval>3));};

E :			T2               	{$<dval>$ = $<dval>1;};

T2 :    		T2 '*' REALNUM    {$<dval>$ = $<dval>1 * $<dval>3; /*printf("Times:  %d\n", $$);*/}
|       		T2 '/' REALNUM    {$<dval>$ = $<dval>1 / $<dval>3;};
|				T1;

T1 :    		T1 '^' REALNUM 	{$<dval>$ = pow($<dval>1,$<dval>3);}
|				REALNUM           {$<dval>$ = $<dval>1;};


REALNUM :	'-' NUM				{$<dval>$ = -$<dval>2;}
| 				NUM					{$<dval>$ = $<dval>1;};

/* Handles assigning into a variable */

NEWVAR:		POSNEWVAR			{ };

/*COMMAND:  PRINT CMDSTRING {$<sval>$ = $<sval>2; printf("%s", $<sval>2);}*/
/*CMDSTRING: LITERALSTR ' ' LITERALSTR {printf("%s %s", $<sval>1, $<sval>3);}*/

COMMAND: 	PRINT CMDSTRING;

CMDSTRING:	CMDSTRING STRTOKEN
| 				STRTOKEN;

STRTOKEN: 	STRTOPRINT { printf("%s", $<sval>1); }
|					E { printf("%g", $<dval>1); }; /* causes shift/reduce error */

%%
