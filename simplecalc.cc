/*
 * CSE 450 Project 3
 * Group Members:
 * Matthew Vorce - vorcemat
 * Andrew Matteson - mattes18
 * William Juszczyk - juszczy1
 *
 * C++ file for expression calculator
 */


#include <cstdio>
#include <iostream>
#include <list>
#include <vector>
#include <string>

using namespace std;
extern "C" {
#include "y.tab.h"
    extern FILE *yyin;
    extern int yylex();
    extern int yyparse();
    extern char *yytext;
}


vector<string> variables;
vector<double> values;


int main(int argc, char **argv)
{
    FILE *file;
    
    if(argc < 2)
    {
        fprintf(stderr, "Using Standard input: Use ^D to exit.\n");
        file = stdin; 
    } else {
        file = fopen(argv[1], "r");
    }
    
    if(!file)
    {
        fprintf(stderr, "Unable to open file %s", argv[1]);
        return 1;
    }
    
    //cout << "Simple 2.0!" << endl;
    
    yyin = file;
    
    do
    {
        yyparse();
    } while(!feof(yyin));
    
    return 0;
}

extern "C" bool CheckVariable(char *s, double *value)
{
	string var(s);
	for(int i = 0; i < variables.size(); i++)
	{
		if(variables[i] == var)
		{
			(*value) = values[i];
			return true;
		}
	
	}
	
	return false;
}

extern "C" void AddNewVariable(char *s, double value)
{
	string var(s);
	for(int i = 0; i < variables.size(); i++)
	{
		if(variables[i] == var)
		{
			variables[i] = var;
			values[i] = value;
			return;
		}
	}
	variables.push_back(var);
	values.push_back(value);
}

extern "C" void yyerror(char *s)
{ 
    cout << s << endl;
}

extern "C" void answer(double n)
{
	cout << "Answer:  " << n << endl;
}

extern "C" void print(char *s)
{
    cout << s << endl;
}
